-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2013 at 07:31 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lawsuit`
--

-- --------------------------------------------------------

--
-- Table structure for table `case`
--

CREATE TABLE IF NOT EXISTS `case` (
  `case_no` varchar(10) NOT NULL,
  `case_title` varchar(30) DEFAULT NULL,
  `case_desc` varchar(255) DEFAULT NULL,
  `case_open_date` date DEFAULT NULL,
  `case_close_date` date DEFAULT NULL,
  PRIMARY KEY (`case_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `case`
--

INSERT INTO `case` (`case_no`, `case_title`, `case_desc`, `case_open_date`, `case_close_date`) VALUES
('1234', 'President  Killed', 'President of India has been kidnepped and killed in Shimla.', '2013-05-27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lawyer_on_case`
--

CREATE TABLE IF NOT EXISTS `lawyer_on_case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_no` varchar(10) NOT NULL,
  `reg_id` varchar(50) NOT NULL,
  `case_feedback` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `lawyer_on_case`
--

INSERT INTO `lawyer_on_case` (`id`, `case_no`, `reg_id`, `case_feedback`, `date`) VALUES
(1, '1234', '1', 'Case Started', '2013-05-27');

-- --------------------------------------------------------

--
-- Table structure for table `lawyer_specialization`
--

CREATE TABLE IF NOT EXISTS `lawyer_specialization` (
  `reg_id` varchar(50) NOT NULL,
  `specialization_id` int(3) NOT NULL,
  PRIMARY KEY (`reg_id`,`specialization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lawyer_specialization`
--


-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(2) NOT NULL,
  `role_desc` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_desc`) VALUES
(1, 'admin'),
(2, 'lawyer'),
(3, 'admin_lawyer');

-- --------------------------------------------------------

--
-- Table structure for table `specialization`
--

CREATE TABLE IF NOT EXISTS `specialization` (
  `specialization_id` int(3) NOT NULL,
  `specialization_desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`specialization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialization`
--


-- --------------------------------------------------------

--
-- Table structure for table `state_district`
--

CREATE TABLE IF NOT EXISTS `state_district` (
  `sd_id` varchar(5) NOT NULL,
  `state` varchar(30) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state_district`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) DEFAULT NULL,
  `lname` varchar(30) DEFAULT NULL,
  `role_id` int(2) NOT NULL,
  `email_id` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `sd_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`reg_id`, `fname`, `lname`, `role_id`, `email_id`, `password`, `sd_id`) VALUES
(1, 'Hridesh', 'Kumar', 1, 'hrideshharsh@yahoo.com', '123', NULL),
(2, 'Piyush', 'Agarwal', 2, 'piyush.agarwal88@gmail.com', '123', NULL),
(3, 'Abhishek', 'Vikram', 3, 'abhi.abhi.com', '123', NULL);
