<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Author: Inventxo
 * Description: Login controller class
 */
 
class Login extends CI_Controller
{
    
    function __construct()
	{
        parent::__construct();
		$this->load->model('login_model');
    }

        
    public function do_login()
	{  
        if(empty($_POST))
        {
        redirect('welcome','refresh');    
        }
        
        // Validate the user
        $result = $this->login_model->validate();
        // Now we verify the result
		
		//echo "<pre>";
		//die(print_r($result, true));
        
		if(!$result)
		{
            // If user did not validate, then show them login page again with message
			$this->session->set_flashdata('lmessage',"<font color = 'red'> &nbsp;Invalid Id and/or Password.</font><br/>");          
			redirect('welcome','refresh');
        }
		else
		{
            // If user did validate, Send them to members area
            redirect('home','refresh');
        }        
    }
	
	public function do_logout()
	{
        $this->session->sess_destroy();     
        redirect('welcome', "refresh");
    }
}
?>