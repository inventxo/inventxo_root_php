<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Author: Inventxo
 * Description: Login controller class
 */
 
class Caselaw extends CI_Controller
{
	function __construct()
	{
        parent::__construct();
		$this->load->model('caselaw_model');
    }
	
	
public function getCaseDetails($case_no ='NULL')
	{	
		if (!empty($_POST['caseno'])) {
			$case_no=$_POST['caseno'];
		}
		$case_details = $this->caselaw_model->get_case_details($case_no);
		
		/*echo "<pre>";
		//die(print_r($case_details, true));
		die(print_r($this->session->all_userdata()));
*/


		if(empty($case_details['case_no']))
		{
		$this->session->set_flashdata('cmessage',"<font color = 'red'> &nbsp;Error : Invalid Case No</font><br/>");          
		redirect('welcome','refresh');
		}

		$data['case_details'] = $case_details;
	
		$data['title'] ="Case Details";
/*
		echo "<pre>";
		die(print_r($data, true));*/

		$this->load->view('templates/header', $data);
		$this->load->view('templates/left', $data);
		$this->load->view('home/case_details', $data);
		$this->load->view('templates/footer');
	}
}