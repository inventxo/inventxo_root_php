	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Author: Inventxo
 *
 * Description: Home controller class
 * This is only viewable to those members that are logged in
 */

 class Home extends CI_Controller
 {
    function __construct()
	{
        parent::__construct();        
        $this->load->model('caselaw_model');
    }
	
	public function index()
	{
		$this->check_isvalidated();
		$data['lawyer_on_case'] = '';
		$data['all_cases'] = '';

		//Check for user role
		if(($this->session->userdata('role_id') == 1) || ($this->session->userdata('role_id') == 3))
		{
		//Get all cases if role is of admin (1 or 3)
		$data['all_cases'] = ''; 
		}
		
		if(($this->session->userdata('role_id') == 2) || ($this->session->userdata('role_id') == 3))
		{
		//Get distinct case numbers on which lawyer is/was associated
		$lawyer_on_case = $this->caselaw_model->get_lawer_on_case($this->session->userdata('reg_id'));
		$data['lawyer_on_case'] = $lawyer_on_case;
		}

	
		$data['title'] ="My Page";
		$data['session_reg_id'] = $this->session->userdata('reg_id');
		$data['lawyer_name'] = $this->session->userdata('fname') ." ". $this->session->userdata('lname');
		
		$this->load->view('templates/header',$data);
		$this->load->view('templates/left',$data);
		$this->load->view('home/my_page',$data);
		$this->load->view('templates/footer',$data);
	
	}
    
    private function check_isvalidated()
	{
        if(! $this->session->userdata('validated'))
		{
            redirect('login', "refresh");
        }
    }    
    
 }
 ?>