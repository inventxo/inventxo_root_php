<?php

class Pages extends CI_Controller {

	function __construct()
	{
        parent::__construct();
        //$this->check_isvalidated();
       // $this->load->model('caselaw_model');
    }


		public function view($page = 'home')
	{
		if($page == 'home' )
		redirect('welcome', "refresh");

		if ( ! file_exists('application/views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data['title'] = ucfirst($page); // Capitalize the first letter
		
		
		$this->load->view('templates/header',$data);
		$this->load->view('templates/left',$data);
		$this->load->view('pages/'.$page,$data);
		$this->load->view('templates/footer',$data);

		/*$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);*/

	}
}