   
<div id="right_column">
	<h1>Services</h1>      
	
	<table cellspacing="0" cellpadding="0" width="100%">
		<tbody>
			<tr>
				<td valign="top" colspan="3" align="center">
					<h3> Dealing Area of Law </h3>
				</td>
			</tr>
			<tr>
				<td valign="top" align="left">
					<p>&nbsp;</p>
					<table border="0" cellspacing="10" cellpadding="5" width="100%">
						<tbody>
							<tr>
								<td class="text" valign="top" >
                                
                                
									<p class="title"><strong>ADMIRALITY &amp; MARITIME </strong></p>
                                    <div id="block">
									<ul>
										<li>COMMERCIAL SHIP ACCIDENT</li>
										<li>SHIP REGISTRATION</li>
										<li>SHIPPING DAMAGE AND LITIGATION</li>
							        </ul>
								    </div>
                                    <br><br>
							      <p class="title"><strong>ANTI TRUST AND TRADE REGULATION</strong> </p>
                                  <div id="block">
									<ul>
										<li>INTERNATIONAL TRADE</li>
								    </ul>
								    </div>
                                    <br><br>
							      <p class="title"><strong>AVIATION LAW</strong> </p>
                                  <div id="block">
									<ul>
										<li>AEROSPACE LAW</li>
										<li>AVIATION ACCIDENTS</li>
										<li>AIRLINE SERVICES CASES</li>
										<li>MISPLACED / DAMAGED BAGAGED CASES </li>
									</ul>
								  </div>
							  </td>
								<td>&nbsp;
								</td>
								<td class="text" valign="top">
                                
									<p class="title"><strong>ESTATE PLANNING</strong> </p>
                                    <div id="block">
									<ul>
										<li>ESTATE &amp; TRUST</li>
										<li>INHERITANCE LAW</li>
										<li>PERSONAL PROPERTY</li>
										<li>INDIAN SUCCESSION LAW</li>
										<li>PROBATE</li>
										<li>WILL</li>
										<li>GENERAL POWER OF ATTORNEY</li>
										<li>CONTRACT AND AGREEMENT</li>
								    </ul>
					                </div>
			                  <br />
                              
										<span class="title"><strong>FAMILY LAW</strong> </span>	
                                        <div id="block">								
									<ul>
										<li>ADOPTION- INTERNATIONAL </li>
										<li>DIVORCE CASES </li>
										<li>DOWRY CASES </li>
										<li>ALIMONY </li>
										<li>CHILD SUPPORT AND CUSTODY </li>
										<li>DOMESTIC VIOLENCE </li>
										<li>PRE-NUPTIAL AGREEMENT</li>
										<li>SEPERATION</li>
										<li>MAINTENANCE</li>
									</ul>
                                    </div>
								</td>
							</tr>
						</tbody>
					</table>
					<font size="3"></font>
				</td>
				<td valign="top" align="left">
					<p>
						<font size="3">&nbsp;
						</font>
					</p>
				</td>
				<td valign="top" align="left">
					<p>
						<font size="3">&nbsp;
						</font>
					</p>
				</td>
			</tr>
		</tbody>
	</table>
</div> <!-- End Column Right-->
           