<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Inventxo
 * Description: Login model class
 */
class Login_model extends CI_Model
{
    function __construct()
	{
    parent::__construct();
    }
    
    public function validate()
	{
        // grab user input
        $e_r_id = $this->security->xss_clean($this->input->post('e_r_id'));
        $password = $this->security->xss_clean($this->input->post('password'));
        
        // Prep the query
        		
		$this->db->where('password', $password);
        $this->db->where('email_id', $e_r_id);
		$this->db->or_where('reg_id', $e_r_id);
        $this->db->where('password', $password);
        
        // Run the query
        $query = $this->db->get('user');
        // Let's check if there are any results
		
		
		
        if($query->num_rows == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();
			
			//echo "<pre>";
		    //die(print_r($row, true));
		
            $data= array(
                    'reg_id' => $row->reg_id,
                    'email_id' => $row->email_id,
					'fname' => $row->fname,
                    'lname' => $row->lname,
                    'role_id' => $row->role_id,
					'validated' => true
                    );
            $this->session->set_userdata($data);
            return true;
        }
        // If the previous process did not validate
        // then return false.
        return false;
    }
}
?>