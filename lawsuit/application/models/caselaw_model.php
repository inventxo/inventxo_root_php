<?php

class Caselaw_model extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
	}


	public function get_case_details($caseno)
	{
		$query1 = $this->db->get_where('case', array('case_no' => $caseno));
		$query2 = $this->db->get_where('lawyer_on_case', array('case_no' => $caseno));
				
		$case_details = $query1->row_array();
		$case_feedbacks = $query2->result_array();
		
		if(!empty($case_feedbacks))
		{
		$count = 0;
		foreach ($case_feedbacks as $row) 
			{			
			$queryTemp = $this->db->get_where('user', array('reg_id' => $row['reg_id']));
			$resultTemp = $queryTemp->row_array();

			$case_feedbacks[$count]['name'] = $resultTemp['fname'].' '.$resultTemp['lname'];

			$count++;
			}
		}

		$case_details['case_feedbacks'] = $case_feedbacks;
		
		//echo "<pre>";
		//die(print_r($case_details, true));
		
		return $case_details;
	}


		//to fetch the lawyer on which case working and show lawyer case at the time of login*/
		public function get_lawer_on_case($reg_id)
		{
					
			$this->db->distinct();
			$this->db->select('case_no');
			$this->db->where('reg_id', $reg_id); 
			$query = $this->db->get('lawyer_on_case');
			$case_details = $query->result_array();


			$count=0;
			$lawyer_on_case ='';
			
			foreach ($case_details as $value) 
			{
				$queryTemp = null;

				$queryTemp = $this->db->get_where('case', array('case_no' => $value['case_no']));
				$resultTemp = $queryTemp->row_array();



				$lawyer_on_case[$count]['case_no'] = $resultTemp['case_no'];		
				$lawyer_on_case[$count]['case_title'] = $resultTemp['case_title'];
				$lawyer_on_case[$count]['case_desc'] = $resultTemp['case_desc'];
				$lawyer_on_case[$count]['case_open_date'] = $resultTemp['case_open_date'];				

				$count++;
			}					
			
			return $lawyer_on_case;
		}
}